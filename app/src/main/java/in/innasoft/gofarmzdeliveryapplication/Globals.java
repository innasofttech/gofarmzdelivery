package in.innasoft.gofarmzdeliveryapplication;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.util.LruCache;
import android.text.format.DateUtils;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.Display;
import android.view.WindowManager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import in.innasoft.gofarmzdeliveryapplication.model.User;

import static android.widget.ImageView.ScaleType;


public class Globals {

    static String stringResponse = null;

    //LIVE///
   /* public static String DEVELOPMENT_BASE_URL = "http://13.232.141.21/delivery/";
    public static String PRODUCTION_BASE_URL = "http://13.232.141.21/delivery/";
    public static String PRODUCTS_IMAGE_URL = "http://13.232.141.21/images/products/400x400/";*/


    //TESTING/////
    public static String DEVELOPMENT_BASE_URL = "http://gofarmzv2.learningslot.in/delivery/";
    public static String PRODUCTION_BASE_URL = "http://gofarmzv2.learningslot.in/delivery/";
    public static String PRODUCTS_IMAGE_URL = "http://gofarmzv2.learningslot.in/images/products/400x400/";


    private static ImageLoader mImageLoader;
    private static Context mCtx;
    private static RequestQueue mRequestQueue;
    static Bitmap responseBitmap;
    public static int MY_SOCKET_TIMEOUT_MS = 60000;

    private static Pattern pattern = Pattern.compile("^[a-zA-Z0-9\\._%+\\-]+@[a-zA-Z0-9\\-]+(\\.[a-zA-Z0-9\\-]+)*$");

    public static User user;

    public Globals(Context context) {
        mCtx = context;
        mRequestQueue = getRequestQueue();
    }

    /*GET METHOD REQUEST FOR API*/
    public static String GET(String url, final Map<String, String> headers, final Map<String, String> params, final VolleyCallback callback) {
        getRequestQueue();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Log.v("Global Response", response + "");
                stringResponse = response;
                callback.onSuccess(stringResponse);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                try {
                    if (volleyError instanceof NetworkError) {
                        //For slow network connection(2Gnet)
                        if (Connectivity.isConnectedFast(mCtx)) {
                            stringResponse = "NetworkError";
                        } else {
                            stringResponse = "Oops slow connection !, please check the network";
                        }
                    } else if (volleyError instanceof ParseError) {
                        stringResponse = "ParseError";
                    } else if (volleyError instanceof ServerError) {
                        stringResponse = "ServerError";
                    } else if (volleyError instanceof AuthFailureError) {
                        stringResponse = "AuthFailureError";
                    } else if (volleyError instanceof NoConnectionError) {
                        stringResponse = "NoConnectionError";
                    } else if (volleyError instanceof TimeoutError) {
                        stringResponse = "TimeoutError";
                    } else {
                        stringResponse = "Error Occurred";
                    }
                } catch (Exception e) {
                    stringResponse = "Error Occurred";
                    //Log.v("exception", "" + e);
                    e.printStackTrace();
                }
                callback.onFail(stringResponse);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Log.v("globals params", "" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
               /* if (Globals.userProfile != null && Globals.userProfile.isAuthenticated)
                    headers.put("token", Globals.userProfile.token);
                else
                    headers.put("token", Globals.apptitudeToken);
                //Log.v("Headers", "" + headers);*/
                return headers;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(MY_SOCKET_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //if cache is enabled
        stringRequest.setShouldCache(false);
        addRequestQueue(stringRequest);

        return stringResponse;
    }

    /*POST METHOD REQUEST FOR API*/
    public static String POST(String url, final Map<String, String> headers, final Map<String, String> params, final VolleyCallback callback) {
        getRequestQueue();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                Log.v("Global Response", response);
                stringResponse = response;
                callback.onSuccess(stringResponse);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                stringResponse = "Connection TimeOut. Please try again.";
                try {
                    if (volleyError instanceof NetworkError) {
                        //For slow network connection(2Gnet)
                        if (Connectivity.isConnectedFast(mCtx)) {
                            stringResponse = "NetworkError";
                        } else {
                            stringResponse = "Oops slow connection !, please check the network";
                        }
                    } else if (volleyError instanceof ParseError) {
                        stringResponse = "ParseError";
                    } else if (volleyError instanceof ServerError) {
                        stringResponse = "ServerError";
                    } else if (volleyError instanceof AuthFailureError) {
                        stringResponse = "AuthFailureError";
                    } else if (volleyError instanceof NoConnectionError) {
                        stringResponse = "NoConnectionError";
                    } else if (volleyError instanceof TimeoutError) {
                        stringResponse = "TimeoutError";
                    } else {
                        stringResponse = "Error Occurred";
                    }
                } catch (Exception e) {
                    stringResponse = "Error Occurred";
                    //Log.v("exception", "" + e);
                    e.printStackTrace();
                }
                callback.onFail(stringResponse);

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
//                Log.v("globals params", "" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                /*if (Globals.userProfile != null && Globals.userProfile.isAuthenticated)
                    headers.put("token", Globals.userProfile.token);
                else
                    headers.put("token", Globals.apptitudeToken);
                Log.v("globals headers", "" + headers);*/
                return headers;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(MY_SOCKET_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //if cache is enabled
        stringRequest.setShouldCache(false);
        addRequestQueue(stringRequest);

        return stringResponse;
    }

    /*POST RAWDATA*/

    public static String POSTRAWDATA(final String url, final Map<String, String> headers, final Map<String, String> params, final String body, final VolleyCallback callback) {
        getRequestQueue();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                Log.v(url + "Global Response", response);
                stringResponse = response;
                callback.onSuccess(stringResponse);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                stringResponse = "Connection TimeOut. Please try again.";
                try {
                    if (volleyError instanceof NetworkError) {
                        stringResponse = "NetworkError";
                    } else if (volleyError instanceof ParseError) {
                        stringResponse = "ParseError";
                    } else if (volleyError instanceof ServerError) {
                        stringResponse = "ServerError";
                    } else if (volleyError instanceof AuthFailureError) {
                        stringResponse = "AuthFailureError";
                    } else if (volleyError instanceof NoConnectionError) {
                        stringResponse = "NoConnectionError";
                    } else if (volleyError instanceof TimeoutError) {
                        stringResponse = "TimeoutError";
                    } else {
                        stringResponse = "Error Occurred";
                    }
                } catch (Exception e) {
                    stringResponse = "Error Occurred";
//                    Log.v("exception", "" + e);
                    e.printStackTrace();
                }
                callback.onFail(stringResponse);

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
//                Log.v("globals params", "" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
//                Log.v("globals headers", "" + headers);
                if (headers == null) {
                }
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
//                Log.v("globals Body", "" + body);
                return body.getBytes();
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(MY_SOCKET_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        addRequestQueue(stringRequest);

        return stringResponse;
    }

    /*IMAGE REQUEST FOR API*/
    public static Bitmap IMAGE(String url, int width, int height, final VolleyImageCallback callback) {
        getRequestQueue();
        ImageRequest imageRequest = new ImageRequest(url, new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap bitmap) {
                responseBitmap = bitmap;
                callback.onSuccess(bitmap);

            }
        }, width, height, null, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                try {
                    if (volleyError instanceof NetworkError) {
                        stringResponse = "NetworkError";
                    } else if (volleyError instanceof ParseError) {
                        stringResponse = "ParseError";
                    } else if (volleyError instanceof ServerError) {
                        stringResponse = "ServerError";
                    } else if (volleyError instanceof AuthFailureError) {
                        stringResponse = "AuthFailureError";
                    } else if (volleyError instanceof NoConnectionError) {
                        stringResponse = "NoConnectionError";
                    } else if (volleyError instanceof TimeoutError) {
                        stringResponse = "TimeoutError";
                    } else {
                        stringResponse = "Error Occurred";
                    }
                } catch (Exception e) {
                    stringResponse = "Error Occurred";
                    //Log.v("exception", "" + e);
                    e.printStackTrace();
                }
                callback.onFail(stringResponse);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                /*if (Globals.userProfile != null && Globals.userProfile.isAuthenticated)
                    headers.put("token", Globals.userProfile.token);
                else
                    headers.put("token", Globals.apptitudeToken);
                Log.v("globals headers", "" + headers);*/
                return headers;
            }
        };
        imageRequest.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        addRequestQueue(imageRequest);
        return responseBitmap;
    }

    /*IMAGE REQUEST FOR API*/
    public static Bitmap IMAGEPOST(String url, int method, int width, int height, ScaleType scaleType, final VolleyImageCallback callback) {
        getRequestQueue();
        if (scaleType == null)
            scaleType = ScaleType.CENTER_INSIDE;
        CustomImageRequest imageRequest = new CustomImageRequest(url, method, new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap bitmap) {
                responseBitmap = bitmap;
                callback.onSuccess(bitmap);

            }
        }, width, height, scaleType, null, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                try {
                    if (volleyError instanceof NetworkError) {
                        stringResponse = "NetworkError";
                    } else if (volleyError instanceof ParseError) {
                        stringResponse = "ParseError";
                    } else if (volleyError instanceof ServerError) {
                        stringResponse = "ServerError";
                    } else if (volleyError instanceof AuthFailureError) {
                        stringResponse = "AuthFailureError";
                    } else if (volleyError instanceof NoConnectionError) {
                        stringResponse = "NoConnectionError";
                    } else if (volleyError instanceof TimeoutError) {
                        stringResponse = "TimeoutError";
                    } else {
                        stringResponse = "Error Occurred";
                    }
                } catch (Exception e) {
                    stringResponse = "Error Occurred";
//                    Log.v("exception", "" + e);
                    e.printStackTrace();
                }
                callback.onFail(stringResponse);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                /*if (Globals.userProfile != null && Globals.userProfile.isAuthenticated)
                    headers.put("token", Globals.userProfile.token);
                else
                    headers.put("token", Globals.apptitudeToken);
                //Log.v("globals headers", "" + headers);*/
                return headers;
            }
        };
        imageRequest.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        addRequestQueue(imageRequest);
        return responseBitmap;
    }

    /*PATCH METHOD REQUEST FOR API*/
    public static String PATCH(String url, final Map<String, String> headers, final Map<String, String> params, final String body, final VolleyCallback callback) {
        getRequestQueue();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Log.v("Global Response", response);
                stringResponse = response;
                callback.onSuccess(stringResponse);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                stringResponse = "Connection TimeOut. Please try again.";
                try {
                    if (volleyError instanceof NetworkError) {
                        stringResponse = "NetworkError";
                    } else if (volleyError instanceof ParseError) {
                        stringResponse = "ParseError";
                    } else if (volleyError instanceof ServerError) {
                        stringResponse = "ServerError";
                    } else if (volleyError instanceof AuthFailureError) {
                        stringResponse = "AuthFailureError";
                    } else if (volleyError instanceof NoConnectionError) {
                        stringResponse = "NoConnectionError";
                    } else if (volleyError instanceof TimeoutError) {
                        stringResponse = "TimeoutError";
                    } else {
                        stringResponse = "Error Occurred";
                    }
                } catch (Exception e) {
                    stringResponse = "Error Occurred";
                    //Log.v("exception", "" + e);
                    e.printStackTrace();
                }
                callback.onFail(stringResponse);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Log.v("globals params", "" + params);
                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                /*Log.v("globals headers", "" + headers);
                if (Globals.userProfile != null && Globals.userProfile.isAuthenticated)
                    headers.put("token", Globals.userProfile.token);
                else
                    headers.put("token", Globals.apptitudeToken);
                headers.put("X-HTTP-Method-Override", "PATCH");*/
                return headers;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(MY_SOCKET_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        addRequestQueue(stringRequest);

        return stringResponse;
    }

    /*DELETE METHOD REQUEST FOR API*/
    public static String DELETE(String url, final Map<String, String> headers, final Map<String, String> params, final String body, final VolleyCallback callback) {
        getRequestQueue();
        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Log.v("Global Response", response);
                stringResponse = response;
                callback.onSuccess(stringResponse);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                stringResponse = "Connection TimeOut. Please try again.";
                try {
                    if (volleyError instanceof NetworkError) {
                        stringResponse = "NetworkError";
                    } else if (volleyError instanceof ParseError) {
                        stringResponse = "ParseError";
                    } else if (volleyError instanceof ServerError) {
                        stringResponse = "ServerError";
                    } else if (volleyError instanceof AuthFailureError) {
                        stringResponse = "AuthFailureError";
                    } else if (volleyError instanceof NoConnectionError) {
                        stringResponse = "NoConnectionError";
                    } else if (volleyError instanceof TimeoutError) {
                        stringResponse = "TimeoutError";
                    } else {
                        stringResponse = "Error Occurred";
                    }
                } catch (Exception e) {
                    stringResponse = "Error Occurred";
                    //Log.v("exception", "" + e);
                    e.printStackTrace();
                }
                callback.onFail(stringResponse);

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Log.v("globals params", "" + params);
                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
               /* Log.v("globals headers", "" + headers);
                if (Globals.userProfile != null && Globals.userProfile.isAuthenticated)
                    headers.put("token", Globals.userProfile.token);
                else
                    headers.put("token", Globals.apptitudeToken);*/
                return headers;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(MY_SOCKET_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        addRequestQueue(stringRequest);

        return stringResponse;
    }

    public interface VolleyCallback {
        void onSuccess(String result);

        void onFail(String result);
    }

    public interface VolleyImageCallback {
        void onSuccess(Bitmap bitmap);

        void onFail(String result);

    }

    public static RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {

            mRequestQueue = Volley.newRequestQueue(mCtx);
        }
        return mRequestQueue;

    }

    public static void addRequestQueue(Request request) {
        getRequestQueue().add(request);
    }

    /*Method For ImageLoader*/
    public static ImageLoader getImageLoader() {
        mImageLoader = new ImageLoader(mRequestQueue,
                new ImageLoader.ImageCache() {
                    private final LruCache<String, Bitmap>
                            cache = new LruBitmapCache();

                    @Override
                    public Bitmap getBitmap(String url) {
                        return cache.get(url);
                    }

                    @Override
                    public void putBitmap(String url, Bitmap bitmap) {
                        cache.put(url, bitmap);
                    }
                });
        return mImageLoader;
    }

    public static Point getScreenSize(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        return point;
    }


    public static String getStringResourceByName(Context context, String aString) {
        Resources resources = context.getResources();
        String PACKAGE_NAME = context.getPackageName();
        int resourceID = resources.getIdentifier(aString, "string", PACKAGE_NAME);
        if (resourceID == 0) {
            return aString;
        } else {
            return resources.getString(resourceID);
        }
    }

    public static Typeface typefaceFor(Context context, String typeface) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/" + typeface);
    }


//    public static Typeface getFontAwesome(Context context) {
//        return Typeface.createFromAsset(context.getAssets(), "fonts/" + context.getResources().getString(R.string.font_awesome));
//    }

    public static String getDeviceName() {
        final String manufacturer = Build.MANUFACTURER;
        final String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return model;
        }
        if (manufacturer.equalsIgnoreCase("HTC")) {
            // make sure "HTC" is fully capitalized.
            return "HTC " + model;
        }
        return manufacturer + " " + model;
    }


    public static String dataStringForImage(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 75, baos); //bitmap is the bitmap object
        byte[] byteArrayImage = baos.toByteArray();
        return "data:image/jpg;base64," + Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
    }

    public static boolean isValidEmailAddress(String email) {
        // return pattern.matcher(email).matches();
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static Bitmap resizeImage(Bitmap realImage) {
        float maxHeight = (float) 960.0;
        float maxWidth = (float) 640.0;
        float ratio = Math.min(
                (float) maxWidth / realImage.getWidth(),
                (float) maxHeight / realImage.getHeight());
        int width = Math.round((float) ratio * realImage.getWidth());
        int height = Math.round((float) ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width, height, true);
        return newBitmap;
    }


    public static boolean isPackageExisted(Activity activity, String targetPackage) {
        List<ApplicationInfo> packages;
        PackageManager pm;

        pm = activity.getPackageManager();
        packages = pm.getInstalledApplications(0);
        for (ApplicationInfo packageInfo : packages) {
            if (packageInfo.packageName.equals(targetPackage))
                return true;
        }
        return false;
    }

    public static CharSequence converteTimestamp(Long milliseconds) {

        return DateUtils.getRelativeTimeSpanString(milliseconds, System.currentTimeMillis(), DateUtils.MINUTE_IN_MILLIS);
    }

    public static boolean isGpsEnabled(Context context) {
        LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public static String getBaseURL() {
        if (BuildConfig.DEBUG)
            return DEVELOPMENT_BASE_URL;
        else
            return PRODUCTION_BASE_URL;
    }

    public static String getEnvironmentName() {
        if (BuildConfig.DEBUG)
            return "development";
        else
            return "production";
    }


    public static String firstLetterUppercase(String response) {
        return Character.toUpperCase(response.charAt(0)) + response.substring(1);
    }

    public static Bitmap convert(Bitmap bitmap, Bitmap.Config config) {
        Bitmap convertedBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), config);
        Canvas canvas = new Canvas(convertedBitmap);
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        canvas.drawBitmap(bitmap, 0, 0, paint);
        return convertedBitmap;
    }

    public static Bitmap compressImage(Context context, Uri imageUri) {
        String filePath = getRealPathFromURI(context, imageUri);
        Bitmap scaledBitmap = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);
        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;
//      max Height and width values of the compressed image is taken as 816x612
        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;
//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image
        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));
//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
            } else if (orientation == 3) {
                matrix.postRotate(180);
            } else if (orientation == 8) {
                matrix.postRotate(270);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Bitmap bitmap1 = convert(scaledBitmap, Bitmap.Config.RGB_565);
        return bitmap1;

    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }
        return inSampleSize;
    }

    public static String getRealPathFromURI(Context context, Uri contentUri) {
        String uriString = String.valueOf(contentUri);
        boolean goForKitKat = uriString.contains("com.android.providers");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && goForKitKat) {
            Log.i("KIKAT", "YES");
            return getPathForV19AndUp(context, contentUri);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return getPathForNaugat(context, contentUri);
        } else {
            return getPathForPreV19(context, contentUri);
        }
    }

    public static String getPathForNaugat(Context context, Uri uri) {
        // just some safety built in
        if (uri == null) {
            // TODO perform some logging or show user feedback
            return null;
        }
        // try to retrieve the image from the media store first
        // this will only work for images selected from gallery
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            String path = cursor.getString(column_index);
            cursor.close();
            return path;
        }
        // this is our fallback here
        return uri.getPath();
    }

    public static String getPathForPreV19(Context context, Uri contentUri) {
        String res = null;

        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();

        return res;
    }

    /**
     * Handles V19 and up uri's
     *
     * @param context
     * @param contentUri
     * @return path
     */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static String getPathForV19AndUp(Context context, Uri contentUri) {
        String wholeID = DocumentsContract.getDocumentId(contentUri);

        // Split at colon, use second item in the array
        String id = wholeID.split(":")[1];
        String[] column = {MediaStore.Images.Media.DATA};

        // where id is equal to
        String sel = MediaStore.Images.Media._ID + "=?";
        Cursor cursor = context.getContentResolver().
                query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        column, sel, new String[]{id}, null);

        String filePath = "";
        int columnIndex = cursor.getColumnIndex(column[0]);
        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filePath;
    }

    public static Uri getImageContentUri(Context context, File imageFile) {
        String filePath = imageFile.getAbsolutePath();
        Cursor cursor = context.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                new String[]{MediaStore.Images.Media._ID},
                MediaStore.Images.Media.DATA + "=? ",
                new String[]{filePath}, null);
        if (cursor != null && cursor.moveToFirst()) {
            int id = cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns._ID));
            cursor.close();
            return Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "" + id);
        } else {
            if (imageFile.exists()) {
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.DATA, filePath);
                return context.getContentResolver().insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            } else {
                return null;
            }
        }
    }

}
