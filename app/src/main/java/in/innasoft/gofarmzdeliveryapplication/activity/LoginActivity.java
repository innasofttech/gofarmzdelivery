package in.innasoft.gofarmzdeliveryapplication.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.HashMap;

import in.innasoft.gofarmzdeliveryapplication.Globals;
import in.innasoft.gofarmzdeliveryapplication.R;

public class LoginActivity extends AppCompatActivity {

    EditText username_edt;
    Button buttonSubmit;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        username_edt = findViewById(R.id.username_edt);
        buttonSubmit = findViewById(R.id.buttonSubmit);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (username_edt.getText().length() > 0) {
                    progressDialog.show();
                    HashMap<String, String> headers = new HashMap<>();
                    HashMap<String, String> params = new HashMap<>();
                    params.put("username", username_edt.getText().toString());

                    Globals.POST(Globals.getBaseURL() + "send_otp", headers, params, new Globals.VolleyCallback() {
                        @Override
                        public void onSuccess(String result) {
                            Log.v("response success", result);
                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(result);
                                if (jsonObject.getString("status").equalsIgnoreCase("10100")) {
                                    Toast.makeText(LoginActivity.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(LoginActivity.this, OTPVerificationActivity.class);
                                    intent.putExtra("username", username_edt.getText().toString());
                                    startActivity(intent);
                                    finish();
                                } else if (jsonObject.getString("status").equalsIgnoreCase("10200")) {
                                    Toast.makeText(LoginActivity.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                                } else if (jsonObject.getString("status").equalsIgnoreCase("10300")) {
                                    Toast.makeText(LoginActivity.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFail(String result) {
                            progressDialog.dismiss();
                            Log.v("failed response", result);

                        }
                    });
                } else {
                    username_edt.setError("Mobile number");
                }

            }
        });
    }
}
