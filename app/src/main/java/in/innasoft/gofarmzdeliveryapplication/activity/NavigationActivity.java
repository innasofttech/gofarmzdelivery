package in.innasoft.gofarmzdeliveryapplication.activity;

import android.Manifest;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.Language;
import com.akexorcist.googledirection.constant.TransportMode;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.model.Route;
import com.akexorcist.googledirection.util.DirectionConverter;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import in.innasoft.gofarmzdeliveryapplication.Connectivity;
import in.innasoft.gofarmzdeliveryapplication.GPSTracker;
import in.innasoft.gofarmzdeliveryapplication.Globals;
import in.innasoft.gofarmzdeliveryapplication.R;
import in.innasoft.gofarmzdeliveryapplication.model.Orders;

public class NavigationActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener, DirectionCallback {

    public static Orders orders;
    private LatLng origin;
    private LatLng destination;
    GPSTracker gpsTracker;
    double latitude = 0;
    double longitude = 0;
    String address;

    Button buttonBack, buttonSubmit, buttonLoation, buttonNumber;
    TextView textTitle, customerName;
    public static final int REQUEST_ID_PERMISSIONS = 1;

    private GoogleMap googleMap;
    private String serverKey = "AIzaSyBTZzHOEs0M-m56yxtYN4aa3gkVHOOMKQE";
    LocationManager locationManager;
    Handler handler = new Handler();
    int delay = 5000; //milliseconds
    Location destinationLocation;
    Location currentLocation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);

        gpsTracker = new GPSTracker(NavigationActivity.this);
        destinationLocation = new Location("Destination");
        currentLocation = new Location("CurrentLocation");


        buttonBack = findViewById(R.id.buttonBack);
        buttonSubmit = findViewById(R.id.buttonSubmit);
        buttonLoation = findViewById(R.id.buttonMap);
        buttonNumber = findViewById(R.id.buttonCall);
        textTitle = findViewById(R.id.textTitle);
        customerName = findViewById(R.id.customerName);

        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);

        textTitle.setText("Navigation");

        if (orders.latitude != null && orders.latitude.length() > 0)
            latitude = Double.parseDouble(orders.latitude);
        if (orders.longitude != null && orders.longitude.length() > 0)
            longitude = Double.parseDouble(orders.longitude);

//        latitude = gpsTracker.getLatitude();
//        longitude = gpsTracker.getLongitude();

        address = getIntent().getStringExtra("address");

        origin = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());
        destination = new LatLng(latitude, longitude);
        destinationLocation.setLatitude(latitude);
        destinationLocation.setLongitude(longitude);
        currentLocation.setLatitude(gpsTracker.getLatitude());
        currentLocation.setLongitude(gpsTracker.getLongitude());

        if (orders != null) {
            customerName.setText(orders.name);
            buttonLoation.setText(orders.address.trim());
            buttonNumber.setText(orders.mobile);
        }

        if (destination.latitude != 0) {
            requestDirection();
        } else {
            destination = getLocationFromAddress(NavigationActivity.this, address);
            requestDirection();
        }


        buttonSubmit.setOnClickListener(this);
        buttonBack.setOnClickListener(this);
        buttonLoation.setOnClickListener(this);
        buttonNumber.setOnClickListener(this);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                2000,
                10, locationListenerGPS);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.getUiSettings().setMapToolbarEnabled(true);
        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                String format = "geo:0,0?q=" + destination.latitude + "," + destination.longitude + "(" + orders.name + ")";
                Uri uri = Uri.parse(format);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.setPackage("com.google.android.apps.maps");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
    }

    LocationListener locationListenerGPS = new LocationListener() {
        @Override
        public void onLocationChanged(android.location.Location location) {

//            Toast.makeText(NavigationActivity.this, "location changed latitude " + location.getLatitude() + " longtiude" + location.getLongitude(), Toast.LENGTH_SHORT).show();

            origin = new LatLng(location.getLatitude(), location.getLongitude());
//            destination = new LatLng(location.getLatitude(), location.getLongitude());
            currentLocation.setLatitude(location.getLatitude());
            currentLocation.setLongitude(location.getLongitude());
            googleMap.clear();
            requestDirection();

//            if (currentLocation.distanceTo(destinationLocation) < 2) {
//                closeApp();
//            }

//            Toast.makeText(NavigationActivity.this, "Distance >> " + currentLocation.distanceTo(destinationLocation), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

//    private void closeApp() {
//        ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
//// The first in the list of RunningTasks is always the foreground task.
//        ActivityManager.RunningTaskInfo foregroundTaskInfo = am.getRunningTasks(1).get(0);
//
//        String foregroundTaskPackageName = foregroundTaskInfo.topActivity.getPackageName();
//        PackageManager pm = getPackageManager();
//        PackageInfo foregroundAppPackageInfo = null;
//        try {
//            foregroundAppPackageInfo = pm.getPackageInfo(foregroundTaskPackageName, 0);
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        }
//        String foregroundTaskAppName = foregroundAppPackageInfo.applicationInfo.loadLabel(pm).toString();
//
//        Log.v("foregroundTaskAppName", foregroundTaskAppName);
//    }


    public void requestDirection() {
        //    Snackbar.make(btnRequestDirection, "Direction Requesting...", Snackbar.LENGTH_SHORT).show();
        GoogleDirection.withServerKey(serverKey)
                .from(origin)
                .to(destination)
                .language(Language.ENGLISH)
                .transportMode(TransportMode.DRIVING)
                .execute(this);
    }

    @Override
    public void onDirectionSuccess(Direction direction, String rawBody) {
//        Toast.makeText(getApplicationContext(), "Success with status : " + direction.getStatus(), Toast.LENGTH_SHORT).show();
        if (direction.isOK()) {
            Route route = direction.getRouteList().get(0);
            googleMap.addMarker(new MarkerOptions().position(origin));
            googleMap.addMarker(new MarkerOptions().position(destination));

            ArrayList<LatLng> directionPositionList = route.getLegList().get(0).getDirectionPoint();
            googleMap.addPolyline(DirectionConverter.createPolyline(this, directionPositionList, 5, Color.RED));
            setCameraWithCoordinationBounds(route);

        } else {
//            Toast.makeText(getApplicationContext(), direction.getStatus(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDirectionFailure(Throwable t) {
        Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
    }

    private void setCameraWithCoordinationBounds(Route route) {
        LatLng southwest = route.getBound().getSouthwestCoordination().getCoordination();
        LatLng northeast = route.getBound().getNortheastCoordination().getCoordination();
        LatLngBounds bounds = new LatLngBounds(southwest, northeast);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonBack:
                finish();
                break;
            case R.id.buttonSubmit:
                AlertDialog.Builder builder = new AlertDialog.Builder(NavigationActivity.this);
                builder.setTitle("Location")
                        .setMessage("Are you reached the location?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                                DeliveryStatus.orders = orders;
                                startActivity(new Intent(NavigationActivity.this, DeliveryStatus.class));
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                                dialog.dismiss();
                            }
                        })
                        .show();
                break;
            case R.id.buttonCall:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkLocationPermission()) {
                        callPhone();
                    }
                } else {
                    callPhone();
                }
                break;
            case R.id.buttonMap:
                if (orders.latitude != null && orders.latitude.length() > 0 && !orders.latitude.equalsIgnoreCase("0")) {
                    latitude = Double.parseDouble(orders.latitude);
                    longitude = Double.parseDouble(orders.longitude);
                    String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%f,%f (%s)", latitude, longitude, orders.name);
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    intent.setPackage("com.google.android.apps.maps");
                    startActivityForResult(intent, 123);
                } else {
                    destination = getLocationFromAddress(NavigationActivity.this, address);
                    String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%f,%f (%s)", destination.latitude, destination.longitude, orders.name);
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    intent.setPackage("com.google.android.apps.maps");
                    startActivityForResult(intent, 123);
                }
                break;
        }
    }

    private void callPhone() {
        Intent intentCall = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + orders.mobile));
        startActivity(intentCall);
    }

    private boolean checkLocationPermission() {
        int locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);

        List<String> listPermissionsNeeded = new ArrayList<>();
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CALL_PHONE);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                    REQUEST_ID_PERMISSIONS);

            return false;
        }
        return true;
    }


    @Override
    protected void onStop() {
//        Toast.makeText(NavigationActivity.this, "On Stop", Toast.LENGTH_SHORT).show();

        handler.postDelayed(new Runnable() {
            public void run() {
                //do something

                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                        2000,
                        10, locationListenerGPS);


                handler.postDelayed(this, delay);
            }
        }, delay);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        handler.removeCallbacks(null);
        locationManager.removeUpdates(locationListenerGPS);
        super.onDestroy();
    }

    public LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }

            Address location = address.get(0);
            p1 = new LatLng(location.getLatitude(), location.getLongitude());

        } catch (IOException ex) {

            ex.printStackTrace();
        }

        return p1;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {
            case REQUEST_ID_PERMISSIONS: {

                Map<String, Integer> perms = new HashMap<>();
                perms.put(Manifest.permission.CALL_PHONE, PackageManager.PERMISSION_GRANTED);

                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);

                    if (perms.get(Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                        callPhone();

                    } else {

                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CALL_PHONE)) {
                            showDialogOK("Call Permissions Required", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    switch (which) {
                                        case DialogInterface.BUTTON_POSITIVE:
                                            checkLocationPermission();
                                            break;

                                        case DialogInterface.BUTTON_NEGATIVE:
                                            break;
                                    }
                                }
                            });
                        } else {
                            Toast.makeText(this, "Goto Settings And Enable Permissions", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        }
    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this).setMessage(message).setPositiveButton("OK", okListener).create().show();
    }
}