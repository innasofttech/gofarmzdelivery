package in.innasoft.gofarmzdeliveryapplication.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.chaos.view.PinView;
import com.stfalcon.smsverifycatcher.OnSmsCatchListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import in.innasoft.gofarmzdeliveryapplication.Globals;
import in.innasoft.gofarmzdeliveryapplication.PrefUtils;
import in.innasoft.gofarmzdeliveryapplication.R;
import in.innasoft.gofarmzdeliveryapplication.utilities.JWTUtils;

public class OTPVerificationActivity extends AppCompatActivity {

    PinView pinView;
    Button buttonSubmit;
    String username;
    ProgressDialog progressDialog;
    SmsVerifyCatcher smsVerifyCatcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otpverification);

        pinView = findViewById(R.id.pinView);
        buttonSubmit = findViewById(R.id.buttonSubmit);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        username = getIntent().getStringExtra("username");


        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (pinView.getText().length() > 0) {
                    progressDialog.show();
                    HashMap<String, String> headers = new HashMap<>();
                    HashMap<String, String> params = new HashMap<>();
                    params.put("username", username);
                    params.put("otp", pinView.getText().toString());

                    Globals.POST(Globals.getBaseURL() + "verify_otp", headers, params, new Globals.VolleyCallback() {
                        @Override
                        public void onSuccess(String result) {
                            Log.v("response success", result);
                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(result);
                                if (jsonObject.getString("status").equalsIgnoreCase("10100")) {
                                    Toast.makeText(OTPVerificationActivity.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                                    JSONObject object = jsonObject.getJSONObject("data");
                                    PrefUtils.saveToLoginPrefs(OTPVerificationActivity.this, username, object.getString("jwt"));
                                    try {
                                        JWTUtils.decoded(object.getString("jwt"));
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    startActivity(new Intent(OTPVerificationActivity.this, MainActivity.class));
                                    finish();

                                } else if (jsonObject.getString("status").equalsIgnoreCase("10200")) {
                                    Toast.makeText(OTPVerificationActivity.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();

                                } else if (jsonObject.getString("status").equalsIgnoreCase("10300")) {
                                    Toast.makeText(OTPVerificationActivity.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();

                                } else if (jsonObject.getString("status").equalsIgnoreCase("10400")) {
                                    Toast.makeText(OTPVerificationActivity.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();

                                } else if (jsonObject.getString("status").equalsIgnoreCase("10500")) {
                                    Toast.makeText(OTPVerificationActivity.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        }

                        @Override
                        public void onFail(String result) {
                            progressDialog.dismiss();
                            Log.v("failed response", result);

                        }
                    });
                } else {
                    Toast.makeText(OTPVerificationActivity.this, "Please enter otp", Toast.LENGTH_SHORT).show();
                }
            }
        });


        smsVerifyCatcher = new SmsVerifyCatcher(this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                String code = message.replaceAll("[^0-9]", "");
                pinView.setText(code);
                Log.v("message >>> ", message.replaceAll("[^0-9]", ""));
            }
        });

    }


    @Override
    protected void onStart() {
        super.onStart();
        smsVerifyCatcher.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        smsVerifyCatcher.onStop();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

}
