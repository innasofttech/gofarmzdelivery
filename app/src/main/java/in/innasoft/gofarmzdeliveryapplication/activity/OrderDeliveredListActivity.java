package in.innasoft.gofarmzdeliveryapplication.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import in.innasoft.gofarmzdeliveryapplication.Connectivity;
import in.innasoft.gofarmzdeliveryapplication.Globals;
import in.innasoft.gofarmzdeliveryapplication.R;
import in.innasoft.gofarmzdeliveryapplication.adapters.DeliveredOrdersAdapter;
import in.innasoft.gofarmzdeliveryapplication.model.DeliverOrderModel;



public class OrderDeliveredListActivity extends AppCompatActivity implements View.OnClickListener
{
    ImageView back_img;
    RecyclerView recyclerDeliveredOrders;
    ProgressDialog progressDialog;
    ArrayList<DeliverOrderModel> deliveredordersArrayList;

    DeliveredOrdersAdapter deliveredOrdersAdapter;
    public static String currency;
    TextView deliver_filter_txt;
    BottomSheetDialog bottomSheetDialog;
    private int birth_Year, birth_Day, birth_Month;
    String sendStartDate,sendEndDate;
    LinearLayoutManager layoutManager;
    int total_number_of_items = 0;
    private boolean userScrolled = true;
    private static int displayedposition = 0;
    int type_of_request = 0;
    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    int defaultPageNo = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_delivered_list);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
        deliveredordersArrayList=new ArrayList<>();

        deliver_filter_txt=findViewById(R.id.deliver_filter_txt);
        deliver_filter_txt.setOnClickListener(this);



        back_img=findViewById(R.id.back_img);
        back_img.setOnClickListener(this);

        recyclerDeliveredOrders=findViewById(R.id.recyclerDeliveredOrders);
        recyclerDeliveredOrders.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(OrderDeliveredListActivity.this);
        recyclerDeliveredOrders.setLayoutManager(layoutManager);
        deliveredOrdersAdapter = new DeliveredOrdersAdapter(OrderDeliveredListActivity.this, deliveredordersArrayList);
        RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(OrderDeliveredListActivity.this, R.drawable.divider));
        recyclerDeliveredOrders.addItemDecoration(dividerItemDecoration);
        recyclerDeliveredOrders.setNestedScrollingEnabled(false);

        recyclerDeliveredOrders.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();
                    displayedposition = pastVisiblesItems;

                    if (loading) {
                        if (userScrolled && (visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            userScrolled = false;
                            defaultPageNo = defaultPageNo + 1;
                            if (totalItemCount < total_number_of_items) {
                                type_of_request = 1;
                                getDeliverOrdersList();
                            } else {
                                Toast.makeText(OrderDeliveredListActivity.this, "No More Results", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    userScrolled = true;
                }
            }
        });
        getDeliverOrdersList();
    }

    private void getDeliverOrdersList()
    {
        if (Globals.user != null)
        {
            progressDialog.show();
            deliveredordersArrayList.clear();
            HashMap<String, String> headers = new HashMap<>();
            HashMap<String, String> params = new HashMap<>();

            String data = "user_id=" + Globals.user.userId + "&page_no="+defaultPageNo;

            Globals.GET(Globals.getBaseURL() + "delivered_orders?" + data, headers, params, new Globals.VolleyCallback() {
                @Override
                public void onSuccess(String result) {
                    Log.d("resultREsponse", result);
                    progressDialog.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("status").equalsIgnoreCase("10100")) {

                            JSONObject object = jsonObject.getJSONObject("data");

                            if (object.has("currency") || !object.isNull("currency"))
                                currency = object.getString("currency");

                            int total_numberof_records = Integer.valueOf(object.getString("recordTotalCnt"));
                            if (total_numberof_records == 0) {
                                Toast.makeText(OrderDeliveredListActivity.this, "No Data Found", Toast.LENGTH_SHORT).show();
                            }
                            total_number_of_items = total_numberof_records;
                            if (total_numberof_records > deliveredordersArrayList.size()) {
                                loading = true;
                            } else {
                                loading = false;
                            }

                            JSONArray jsonArray = object.getJSONArray("recordData");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                DeliverOrderModel deliverOrderModel = new DeliverOrderModel(jsonArray.getJSONObject(i));
                                deliveredordersArrayList.add(deliverOrderModel);
                            }

                            recyclerDeliveredOrders.setAdapter(deliveredOrdersAdapter);



                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFail(String result) {
                    Log.v("resultREsponseFF", result);
                    progressDialog.dismiss();
                }
            });
        }
    }

    @Override
    public void onClick(View view) {
        if(view==back_img)
        {
            finish();
        }
        if(view==deliver_filter_txt)
        {
            bottomSheetDialog = new BottomSheetDialog(OrderDeliveredListActivity.this);
            View v = getLayoutInflater().inflate(R.layout.filter_custom_dialog, null);
            bottomSheetDialog.setContentView(v);
            final TextView starttitle_text,endtitle_text,startDate_text,endDate_text,applydialogbtn;

            starttitle_text=v.findViewById(R.id.starttitle_text);
            endtitle_text=v.findViewById(R.id.endtitle_text);
            startDate_text=v.findViewById(R.id.startDate_text);
            endDate_text=v.findViewById(R.id.endDate_text);
            applydialogbtn=v.findViewById(R.id.applydialogbtn);
            starttitle_text.setOnClickListener(this);
            endtitle_text.setOnClickListener(this);
            applydialogbtn.setOnClickListener(this);


            starttitle_text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {

                    final Calendar c = Calendar.getInstance();
                    birth_Year = c.get(Calendar.YEAR);
                    birth_Month = c.get(Calendar.MONTH);
                    birth_Day = c.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog datePickerDialog = new DatePickerDialog(OrderDeliveredListActivity.this, R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            String strMonth = null, strDay = null;
                            int month = monthOfYear + 1;
                            if (month < 10) {
                                strMonth = "0" + month;
                            } else {
                                strMonth = month + "";
                            }
                            if (dayOfMonth < 10) {

                                strDay = "0" + dayOfMonth;
                            } else {
                                strDay = dayOfMonth + "";
                            }

                            startDate_text.setText(String.valueOf(strDay + "-" + strMonth + "-" + year));
                            sendStartDate=String.valueOf(strDay + "-" + strMonth + "-" + year);
                        }
                    }, birth_Year, birth_Month, birth_Day);

                    datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                    datePickerDialog.show();

                }
            });
            endtitle_text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {

                    final Calendar c = Calendar.getInstance();
                    birth_Year = c.get(Calendar.YEAR);
                    birth_Month = c.get(Calendar.MONTH);
                    birth_Day = c.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog datePickerDialog = new DatePickerDialog(OrderDeliveredListActivity.this, R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            String strMonth = null, strDay = null;
                            int month = monthOfYear + 1;
                            if (month < 10) {
                                strMonth = "0" + month;
                            } else {
                                strMonth = month + "";
                            }
                            if (dayOfMonth < 10) {

                                strDay = "0" + dayOfMonth;
                            } else {
                                strDay = dayOfMonth + "";
                            }

                            endDate_text.setText(String.valueOf(strDay + "-" + strMonth + "-" + year));
                            sendEndDate=String.valueOf(strDay + "-" + strMonth + "-" + year);
                        }
                    }, birth_Year, birth_Month, birth_Day);

                    datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                    datePickerDialog.show();
                }
            });

            applydialogbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    bottomSheetDialog.dismiss();
                    if (Connectivity.isConnected(OrderDeliveredListActivity.this)) {
                        deliveredOrdersAdapter = new DeliveredOrdersAdapter(OrderDeliveredListActivity.this, deliveredordersArrayList);
                        LinearLayoutManager layoutManager = new LinearLayoutManager(OrderDeliveredListActivity.this, LinearLayoutManager.VERTICAL, false);
                        recyclerDeliveredOrders.setLayoutManager(layoutManager);
                        RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(OrderDeliveredListActivity.this, R.drawable.divider));
                        recyclerDeliveredOrders.addItemDecoration(dividerItemDecoration);
                        getOrdersList();
                    } else {
                        Snackbar snackbar = Snackbar.make(OrderDeliveredListActivity.this.getWindow().getDecorView().getRootView(), "No Internet Connection...!", Snackbar.LENGTH_LONG);
                        snackbar.show();

                    }
                }
            });


            bottomSheetDialog.show();
        }
    }


    private void getOrdersList()
    {
        if (Globals.user != null)
        {
            progressDialog.show();
            deliveredordersArrayList.clear();
            HashMap<String, String> headers = new HashMap<>();
            HashMap<String, String> params = new HashMap<>();

            String data = "user_id=" + Globals.user.userId + "&page_no=1"+"&fdate="+sendStartDate+"&tdate="+sendEndDate;
            Log.d("UUUUU",Globals.getBaseURL() + "delivered_orders?" + data);
            Globals.GET(Globals.getBaseURL() + "delivered_orders?" + data, headers, params, new Globals.VolleyCallback() {
                @Override
                public void onSuccess(String result)
                {
                    Log.d("result", result);
                    progressDialog.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("status").equalsIgnoreCase("10100"))
                        {

                            JSONObject object = jsonObject.getJSONObject("data");
                            if (object.has("currency") || !object.isNull("currency"))
                                currency = object.getString("currency");

                            JSONArray jsonArray = object.getJSONArray("recordData");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                DeliverOrderModel deliverOrderModel = new DeliverOrderModel(jsonArray.getJSONObject(i));
                                deliveredordersArrayList.add(deliverOrderModel);

                            }

                            recyclerDeliveredOrders.setAdapter(deliveredOrdersAdapter);
                        }
                        if(jsonObject.getString("status").equalsIgnoreCase("10300"))
                        {
                            deliveredordersArrayList.clear();

                            Toast.makeText(OrderDeliveredListActivity.this,"No Delivered Data Found..!",Toast.LENGTH_LONG);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFail(String result) {
                    Log.v("resultFF", result);
                    progressDialog.dismiss();
                }
            });
        }
    }


}
