package in.innasoft.gofarmzdeliveryapplication.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.innasoft.gofarmzdeliveryapplication.Connectivity;
import in.innasoft.gofarmzdeliveryapplication.Globals;
import in.innasoft.gofarmzdeliveryapplication.R;
import in.innasoft.gofarmzdeliveryapplication.adapters.OrderViewAdapter;
import in.innasoft.gofarmzdeliveryapplication.database.OrdersDbHelper;
import in.innasoft.gofarmzdeliveryapplication.model.OrderView;
import in.innasoft.gofarmzdeliveryapplication.model.ProductView;

public class OrderDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView vieworder_recylerview;
    ProgressDialog progressDialog;
    String orders_pid, referenceId;
    Typeface typeface, typeface2;
    OrderViewAdapter orderViewAdapter;
    ArrayList<ProductView> orderViewModels = new ArrayList<>();
    ArrayList<OrderView> orderViewList = new ArrayList<>();
    TextView textTitle, order_id_text, final_price_text, delivery_address_text_title, delivery_address,
            order_details_text, order_ref_id, order_date_text, product_details_text;
    Toolbar toolbar;
    Button buttonBack;

    OrdersDbHelper ordersDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);

        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.fonttype_one));
        typeface2 = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.fonttype_two));

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        ordersDbHelper = new OrdersDbHelper(this);

        toolbar = findViewById(R.id.toolbar);
        textTitle = findViewById(R.id.textTitle);
        buttonBack = findViewById(R.id.buttonBack);


        orders_pid = getIntent().getStringExtra("id");
        referenceId = getIntent().getStringExtra("ReferenceId");

        order_id_text = findViewById(R.id.order_id);
        final_price_text = findViewById(R.id.final_price);
        delivery_address_text_title = findViewById(R.id.delivery_address_text_title);
        order_details_text = findViewById(R.id.order_details_text);
        product_details_text = findViewById(R.id.product_details_text);
        order_date_text = findViewById(R.id.order_date);
        order_ref_id = findViewById(R.id.order_ref_id);
        delivery_address = findViewById(R.id.delivery_address);

        vieworder_recylerview = findViewById(R.id.vieworder_recylerview);


        textTitle.setTypeface(typeface);
        order_id_text.setTypeface(typeface);
        final_price_text.setTypeface(typeface2);
        delivery_address_text_title.setTypeface(typeface2);
        order_details_text.setTypeface(typeface2);
        product_details_text.setTypeface(typeface2);
        order_date_text.setTypeface(typeface);
        order_ref_id.setTypeface(typeface);
        delivery_address.setTypeface(typeface);

        textTitle.setText(referenceId);


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(OrderDetailsActivity.this);
        vieworder_recylerview.setLayoutManager(layoutManager);
        vieworder_recylerview.setNestedScrollingEnabled(false);
        orderViewAdapter = new OrderViewAdapter(orderViewModels, OrderDetailsActivity.this, R.layout.row_vieworder);
        RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(OrderDetailsActivity.this, R.drawable.divider));
        vieworder_recylerview.addItemDecoration(dividerItemDecoration);

        orderDetails();

        buttonBack.setOnClickListener(this);

    }

    private void orderDetails() {
        orderViewModels.clear();
        if (Connectivity.isConnected(OrderDetailsActivity.this)) {
            progressDialog.show();

            HashMap<String, String> headers = new HashMap<>();
            HashMap<String, String> params = new HashMap<>();
            Log.d("xxxx",Globals.getBaseURL() + "orders/" + orders_pid);
            Globals.GET(Globals.getBaseURL() + "orders/" + orders_pid, headers, params, new Globals.VolleyCallback() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onSuccess(String result) {
                    Log.v("details response ", result);
                    progressDialog.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("status").equalsIgnoreCase("10100")) {
                            JSONObject dataObject = jsonObject.getJSONObject("data");
                            JSONArray orderDetails = dataObject.getJSONArray("order_details");
                            for (int i = 0; i < orderDetails.length(); i++) {
                                OrderView orderView = new OrderView(orderDetails.getJSONObject(i));
                                orderViewList.add(orderView);
                            }

                            delivery_address.setText(orderViewList.get(0).name
                                    + "\n" + orderViewList.get(0).mobile + "\n" +
                                    orderViewList.get(0).address + ", " +
                                    orderViewList.get(0).area + "\n" +
                                    orderViewList.get(0).city + ", " +
                                    orderViewList.get(0).state + "\n" +
                                    orderViewList.get(0).country + "\n" + "Pincode : " +
                                    orderViewList.get(0).pincode);

                            final_price_text.setText("Final Price : \u20B9 " + orderViewList.get(0).final_price);

                            order_ref_id.setText("ORDER REF ID - " + orderViewList.get(0).reference_id);
                            order_date_text.setText("ORDER DATE - " + orderViewList.get(0).order_date);


                            JSONArray productDetails = dataObject.getJSONArray("product_details");
                            for (int i = 0; i < productDetails.length(); i++) {
                                ProductView productView = new ProductView(productDetails.getJSONObject(i));
                                orderViewModels.add(productView);
                            }
                            vieworder_recylerview.setAdapter(orderViewAdapter);

                            saveToDB();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFail(String result) {
                    Log.v("details fail ", result);
                    progressDialog.dismiss();
                }
            });

        } else {
            Snackbar snackbar = Snackbar.make(getWindow().getDecorView().getRootView(), "No Internet Connection...!", Snackbar.LENGTH_LONG);
            snackbar.show();
            saveToDB();
        }
    }

    private void saveToDB() {

        if (Connectivity.isConnected(OrderDetailsActivity.this)) {

            ordersDbHelper.deleteOrderViewList();

            for (int i = 0; i < orderViewModels.size(); i++) {

                ContentValues values = new ContentValues();
                values.put("productName", orderViewModels.get(i).product_name);
                values.put("quantity", orderViewModels.get(i).purchase_quantity);
                values.put("price", orderViewModels.get(i).product_mrp_price);
                values.put("totalPrice", final_price_text.getText().toString());
                values.put("address", delivery_address.getText().toString());
                ordersDbHelper.addOrderViewList(values);
            }

        } else {

            orderViewModels.clear();

            List<String> productNameList = ordersDbHelper.getProductName();
            List<String> quantityList = ordersDbHelper.getQuantity();
            List<String> priceList = ordersDbHelper.getPriceDetail();

            for (int i = 0; i < productNameList.size() ; i++) {
                ProductView orders = new ProductView();
                orders.product_name = productNameList.get(i);
                orders.purchase_quantity = quantityList.get(i);
                orders.product_mrp_price = priceList.get(i);
                orderViewModels.add(orders);

            }

            final_price_text.setText(ordersDbHelper.getTotalPrice().get(0));
            delivery_address.setText(ordersDbHelper.getAddress().get(0));

            vieworder_recylerview.setAdapter(orderViewAdapter);

            ordersDbHelper.close();

        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonBack:
                finish();
                break;
        }
    }
}
