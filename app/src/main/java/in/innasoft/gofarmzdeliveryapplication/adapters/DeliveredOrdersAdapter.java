package in.innasoft.gofarmzdeliveryapplication.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import in.innasoft.gofarmzdeliveryapplication.Connectivity;
import in.innasoft.gofarmzdeliveryapplication.Globals;
import in.innasoft.gofarmzdeliveryapplication.R;
import in.innasoft.gofarmzdeliveryapplication.activity.NavigationActivity;
import in.innasoft.gofarmzdeliveryapplication.activity.OrderDetailsActivity;
import in.innasoft.gofarmzdeliveryapplication.model.DeliverOrderModel;
import in.innasoft.gofarmzdeliveryapplication.model.Orders;

public class DeliveredOrdersAdapter extends RecyclerView.Adapter<DeliveredOrdersAdapter.MyViewHolder> {

    Context context;
    ArrayList<DeliverOrderModel> deliverordersArrayList;

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView orderId, location, totalPrice, amtStatus, customerName, textRemark;
        ImageView productImage, imageInfo, imageNavigate, imageCall;
        Button acceptOrder;

        public MyViewHolder(View view) {
            super(view);
            orderId = view.findViewById(R.id.orderId);
            location = view.findViewById(R.id.location);
            totalPrice = view.findViewById(R.id.total_price);
            amtStatus = view.findViewById(R.id.amtStatus);
            customerName = view.findViewById(R.id.customerName);
            textRemark = view.findViewById(R.id.textRemark);
            productImage = view.findViewById(R.id.productImage);
            imageInfo = view.findViewById(R.id.info);
            imageCall = view.findViewById(R.id.callIcon);
            imageNavigate = view.findViewById(R.id.navGoogleMaps);
            acceptOrder = view.findViewById(R.id.acceptOrder);
        }
    }

    public DeliveredOrdersAdapter(Context context, ArrayList<DeliverOrderModel> orders) {
        this.context = context;
        this.deliverordersArrayList = orders;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_deliverd_orders, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final DeliverOrderModel order = deliverordersArrayList.get(position);

        if (order.name != null && !order.name.isEmpty()){
            holder.customerName.setText(Globals.firstLetterUppercase(order.name));
        } else {
            holder.customerName.setText("");
        }

        if (order.orderStatus != null && !order.orderStatus.isEmpty()){
            holder.textRemark.setText(order.orderStatus);
        } else {
            holder.textRemark.setText("");
        }

        if (order.images != null && !order.images.isEmpty())
            Picasso.with(context).load(order.images).error(R.mipmap.ic_launcher_round).placeholder(R.mipmap.ic_launcher_round).into(holder.productImage);
        else
            holder.productImage.setImageResource(R.mipmap.ic_launcher_round);

        if (order.referenceId != null && order.referenceId.length() > 0) {
            holder.orderId.setText("Order Id : "+order.referenceId);
        } else {
            holder.orderId.setText("");
        }

        if (order.finalPrice != null && order.finalPrice.length() > 0){
            holder.totalPrice.setText("\u20B9 "+order.finalPrice);
        } else {
            holder.totalPrice.setText("");
        }

        if (order.area != null && order.area.length() > 0) {
            holder.location.setText(order.area);
        } else {
            holder.location.setText("");
        }

        if (order.getawayName != null && order.getawayName.length() > 0){
            holder.amtStatus.setText("Paid via : "+order.getawayName);
        } else {
            holder.amtStatus.setText("");
        }


    }



    @Override
    public int getItemCount() {
        return deliverordersArrayList.size();
    }
}
