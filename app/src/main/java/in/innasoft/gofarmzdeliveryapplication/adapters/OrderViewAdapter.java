package in.innasoft.gofarmzdeliveryapplication.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.innasoft.gofarmzdeliveryapplication.R;
import in.innasoft.gofarmzdeliveryapplication.activity.OrderDetailsActivity;
import in.innasoft.gofarmzdeliveryapplication.holders.OrderViewHolder;
import in.innasoft.gofarmzdeliveryapplication.itemClickListerners.OrderViewItemClickListener;
import in.innasoft.gofarmzdeliveryapplication.model.ProductView;

public class OrderViewAdapter extends RecyclerView.Adapter<OrderViewHolder>{

    private ArrayList<ProductView> orderViewModels;
    public OrderDetailsActivity context;
    private LayoutInflater li;
    private int resource;
    private Typeface typeface;

    public OrderViewAdapter(ArrayList<ProductView> orderViewModels, OrderDetailsActivity context, int resource) {
        this.orderViewModels = orderViewModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.fonttype_one));
    }

    @NonNull
    @Override
    public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,parent,false);
        return new OrderViewHolder(layout);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull OrderViewHolder holder, final int position) {

        holder.product_name_txt.setText(orderViewModels.get(position).product_name);
        holder.product_name_txt.setTypeface(typeface);

        holder.quantity_txt.setText("Quantity : "+orderViewModels.get(position).purchase_quantity);
        holder.quantity_txt.setTypeface(typeface);

        holder.total_price_txt.setText("\u20B9"+orderViewModels.get(position).product_mrp_price);
        holder.total_price_txt.setTypeface(typeface);

        Picasso.with(context)
                .load(orderViewModels.get(position).image)
                .placeholder(R.mipmap.ic_launcher)
                .into(holder.order_view_product_img);

        holder.setItemClickListener(new OrderViewItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return this.orderViewModels.size();
    }
}
