package in.innasoft.gofarmzdeliveryapplication.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import in.innasoft.gofarmzdeliveryapplication.R;
import in.innasoft.gofarmzdeliveryapplication.itemClickListerners.OrderViewItemClickListener;

public class OrderTrackHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public TextView order_status,order_message,order_date_and_time;
    OrderViewItemClickListener orderViewItemClickListener;

    public OrderTrackHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        order_status = (TextView) itemView.findViewById(R.id.order_status);
        order_message = (TextView) itemView.findViewById(R.id.order_message);
        order_date_and_time = (TextView) itemView.findViewById(R.id.order_date_and_time);

    }

    @Override
    public void onClick(View view) {
        this.orderViewItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(OrderViewItemClickListener ic)
    {
        this.orderViewItemClickListener =ic;
    }
}
