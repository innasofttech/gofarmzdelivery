package in.innasoft.gofarmzdeliveryapplication.holders;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.innasoft.gofarmzdeliveryapplication.R;
import in.innasoft.gofarmzdeliveryapplication.itemClickListerners.OrderViewItemClickListener;

public class OrderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView product_name_txt, quantity_txt, total_price_txt;

    public ImageView order_view_product_img;
    OrderViewItemClickListener orderViewItemClickListener;

    public OrderViewHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);


        product_name_txt = (TextView) itemView.findViewById(R.id.product_name_txt);

        quantity_txt = (TextView) itemView.findViewById(R.id.quantity_txt);

        total_price_txt = (TextView) itemView.findViewById(R.id.total_price_txt);
        order_view_product_img = (ImageView) itemView.findViewById(R.id.order_view_product_img);

    }

    @Override
    public void onClick(View view) {
        this.orderViewItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(OrderViewItemClickListener ic) {
        this.orderViewItemClickListener = ic;
    }
}
