package in.innasoft.gofarmzdeliveryapplication.model;

import org.json.JSONObject;

import in.innasoft.gofarmzdeliveryapplication.Globals;
import in.innasoft.gofarmzdeliveryapplication.utilities.AppUrls;

public class Orders {

    public String id, name, mobile, referenceId, orderId, finalPrice, orderStatus, status, orderDate, orderTime, latitude, longitude, getawayName, address,
            area, pinCode, distance, images, message_delivery;
    public boolean acceptOrders;

    public Orders(){

    }


    public Orders(JSONObject jsonObject) {
        try {

            if (jsonObject.has("id") && !jsonObject.isNull("id"))
                this.id = jsonObject.getString("id");
            if (jsonObject.has("name") && !jsonObject.isNull("name"))
                this.name = jsonObject.getString("name");
            if (jsonObject.has("mobile") && !jsonObject.isNull("mobile"))
                this.mobile = jsonObject.getString("mobile");
            if (jsonObject.has("reference_id") && !jsonObject.isNull("reference_id"))
                this.referenceId = jsonObject.getString("reference_id");
            if (jsonObject.has("order_id") && !jsonObject.isNull("order_id"))
                this.orderId = jsonObject.getString("order_id");
            if (jsonObject.has("final_price") && !jsonObject.isNull("final_price"))
                this.finalPrice = jsonObject.getString("final_price");
            if (jsonObject.has("order_status") && !jsonObject.isNull("order_status"))
                this.orderStatus = jsonObject.getString("order_status");
            if (jsonObject.has("status") && !jsonObject.isNull("status"))
                this.status = jsonObject.getString("status");
            if (jsonObject.has("order_date") && !jsonObject.isNull("order_date"))
                this.orderDate = jsonObject.getString("order_date");
            if (jsonObject.has("order_time") && !jsonObject.isNull("order_time"))
                this.orderTime = jsonObject.getString("order_time");
            if (jsonObject.has("latitude") && !jsonObject.isNull("latitude"))
                this.latitude = jsonObject.getString("latitude");
            if (jsonObject.has("longitude") && !jsonObject.isNull("longitude"))
                this.longitude = jsonObject.getString("longitude");
            if (jsonObject.has("getaway_name") && !jsonObject.isNull("getaway_name"))
                this.getawayName = jsonObject.getString("getaway_name");
            if (jsonObject.has("address") && !jsonObject.isNull("address"))
                this.address = jsonObject.getString("address");
            if (jsonObject.has("area") && !jsonObject.isNull("area"))
                this.area = jsonObject.getString("area");
            if (jsonObject.has("pincode") && !jsonObject.isNull("pincode"))
                this.pinCode = jsonObject.getString("pincode");
            if (jsonObject.has("distance") && !jsonObject.isNull("distance"))
                this.distance = jsonObject.getString("distance");
            if (jsonObject.has("images") && !jsonObject.isNull("images"))
                this.images = Globals.PRODUCTS_IMAGE_URL + jsonObject.getString("images");
            if (jsonObject.has("message_delivery") && !jsonObject.isNull("message_delivery"))
                this.message_delivery = jsonObject.getString("message_delivery");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
