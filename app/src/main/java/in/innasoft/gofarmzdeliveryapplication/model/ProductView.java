package in.innasoft.gofarmzdeliveryapplication.model;

import org.json.JSONObject;

import in.innasoft.gofarmzdeliveryapplication.Globals;

public class ProductView {

    public String product_name, weight_name, product_mrp_price, purchase_quantity, image;

    public ProductView(){

    }

    public ProductView(JSONObject jsonObject) {
        try {

            if (jsonObject.has("product_name") && !jsonObject.isNull("product_name"))
                this.product_name = jsonObject.getString("product_name");
            if (jsonObject.has("weight_name") && !jsonObject.isNull("weight_name"))
                this.weight_name = jsonObject.getString("weight_name");
            if (jsonObject.has("product_mrp_price") && !jsonObject.isNull("product_mrp_price"))
                this.product_mrp_price = jsonObject.getString("product_mrp_price");
            if (jsonObject.has("purchase_quantity") && !jsonObject.isNull("purchase_quantity"))
                this.purchase_quantity = jsonObject.getString("purchase_quantity");
            if (jsonObject.has("image") && !jsonObject.isNull("image"))
                this.image = Globals.PRODUCTS_IMAGE_URL + jsonObject.getString("image");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}